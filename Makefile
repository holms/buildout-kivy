# WARNING: Tab sensetive file!

.PHONY: clean all
all: develop

develop: clean virtualenv  bootstrap buildout warning

virtualenv:
	virtualenv --no-site-packages .

bootstrap:
	-mkdir eggs
	-rm bootstrap.py
	curl -O http://www.python-distribute.org/bootstrap.py
	-python bootstrap.py

deps:
	#sudo port -v install libsdl libsdl_image libsdl_mixer libsdl_ttf smpeg portmidi jpeg
	#sudo port -v install ffmpeg +nonfree


buildout:
	bin/buildout -c develop.cfg -N
	
.PHONY: syncdb
syncdb:
	bin/django syncdb

.PHONY: run
run:
	bin/django runserver

warning:
	@echo "-------------------------------------------------------"
	@echo "Don't forget to activate virtualenv: . bin/activate"
	@echo "-------------------------------------------------------"

clean:
	-rm -rf bin
	-rm -rf include
	-rm -rf lib
	-rm -rf build
	-rm -rf log
	-rm -rf share
	-rm -rf eggs
	-rm -rf develop-eggs
	-rm -rf .installed.cfg
	-rm -rf parts
	-rm -rf downloads
	-rm -rf src
	-rm -rf local
