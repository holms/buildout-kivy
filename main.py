import os

import kivy
kivy.require('1.4.0')

from kivy.app import App
from kivy.uix.widget import Widget

class MyPaintWidget(Widget):
    def on_touch_down(self, touch):
        with self.canvas:
            Color(1, 1, 0)
            d = 30
            Ellipse(pos=(touch.x - d / 2, touch.y - d / 2), size=(d, d))


class MyPaintApp(App):
    def build(self):
        return MyPaintWidget();

if __name__ == '__main__':
        MyPaintApp().run()
