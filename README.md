Kivy environment with Buildout under Virtualenv
===============================================

Requirements:
-------------

* python-dev (on debian)
* curl
* virtualenv
* virtualenvwrapper
* mercurial


Just do this:

	:::bash
	make virtualenv
	. bin/activate
	make
	kivy main.py

To clean mess:

	:::bash
	make clean

If you doing a git-pull or just exited virtualenv and made make-clean then generate new one:

	:::bash
	make virtualenv
